import { Component, Inject, Renderer2, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isAuthenticated: boolean;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.authService.isAuthenticated.subscribe((credentials) => {
      this.isAuthenticated = credentials;
    });
    this.authService.isLoggedIn().subscribe((resp) => {
      console.log(`              
        +-+ +-+-+-+ +-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+ +-+-+-+ +-+-+-+-+-+-+-+
        |I| |s|e|e| |y|o|u| |s|n|e|a|k|i|n|g| |i|n| |t|h|e| |c|o|n|s|o|l|e|
        +-+ +-+-+-+ +-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+ +-+-+-+ +-+-+-+-+-+-+-+
        ___________________________________  
        | _____ |   | ___ | ___ ___ | |   | |
        | |   | |_| |__ | |_| __|____ | | | |
        | | | |_________|__ |______ |___|_| |
        | |_|   | _______ |______ |   | ____|
        | ___ | |____ | |______ | |_| |____ |
        |___|_|____ | |   ___ | |________ | |
        |   ________| | |__ | |______ | | | |
        | | | ________| | __|____ | | | __| |
        |_| |__ |   | __|__ | ____| | |_| __|
        |   ____| | |____ | |__ |   |__ |__ |
        | |_______|_______|___|___|___|_____|     
        
        +-+-+-+-+-+-+ +-+-+-+ +-+-+-+ +-+-+-+-+-+-+ +-+-+-+-+-+-+
        |A|t|h|e|n|a| |A|p|p| |b|y|:| |M|i|c|h|e|l| |A|l|d|a|i|r|
        +-+-+-+-+-+-+ +-+-+-+ +-+-+-+ +-+-+-+-+-+-+ +-+-+-+-+-+-+ 
      `);
    });
  }

  switchMode(isDarkMode: boolean) {
    const hostClass = isDarkMode ? 'theme-dark' : 'theme-light';
    this.renderer.setAttribute(this.document.body, 'class', hostClass);
  }
}
