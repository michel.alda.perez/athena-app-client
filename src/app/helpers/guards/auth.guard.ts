import { Injectable } from '@angular/core';
import {
  CanLoad,
  Route,
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { take, skipWhile, tap } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (route.parent.queryParams.authenticated) {
      return true;
    }
    return this.authService.isAuthenticated.pipe(
      skipWhile((authenticated) => {
        return authenticated === null;
      }),
      take(1),
      tap((authenticated) => {
        if (!authenticated) {
          return this.router.navigateByUrl('/login');
        }
      })
    );
  }
}
