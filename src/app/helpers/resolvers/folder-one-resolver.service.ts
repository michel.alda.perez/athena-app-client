import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Folder } from '../../interfaces/Folder.interface';
import { Observable } from 'rxjs';
import { FoldersService } from '../../services/folders.service';

@Injectable({
  providedIn: 'root',
})
export class FolderOneResolverService implements Resolve<Folder> {
  constructor(private foldersService: FoldersService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Folder | Observable<Folder> | Promise<Folder> {
    let id = route.params['id'];
    return this.foldersService.getFolder(id);
  }
}
