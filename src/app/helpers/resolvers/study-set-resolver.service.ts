import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute,
} from '@angular/router';
import { Observable } from 'rxjs';
import { StudySet } from '../../interfaces/StudySet.interface';
import { StudySetsService } from '../../services/study-sets.service';

@Injectable({
  providedIn: 'root',
})
export class StudySetResolverService implements Resolve<StudySet[]> {
  constructor(private studySetsService: StudySetsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): StudySet[] | Observable<StudySet[]> | Promise<StudySet[]> {
    return this.studySetsService.getStudySets();
  }
}
