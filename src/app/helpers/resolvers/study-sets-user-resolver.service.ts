import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StudySet } from '../../interfaces/StudySet.interface';
import { StudySetsService } from '../../services/study-sets.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StudySetsUserResolverService implements Resolve<StudySet[]> {
  constructor(private studySetsService: StudySetsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): StudySet[] | Observable<StudySet[]> | Promise<StudySet[]> {
    return this.studySetsService.getUserStudySets();
  }
}

