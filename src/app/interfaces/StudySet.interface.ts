import { User } from './User.interface';
import { StudyCard } from './StudyCard.interface';
import { Folder } from './Folder.interface';

export interface StudySet {
  id: string;
  title: string;
  description: string;
  visibility: string;
  creation_date: Date;
  completation_date?: Date;
  grade: number;
  original_user_id: string;
  user: string;
  study_cards?: StudyCard[];
  folder?: Folder;
}

export interface ResponseGetStudySets {
  id: string;
  title: string;
  description: string;
  visibility: string;
  creation_date: Date;
  completation_date: null;
  grade: number;
  original_user_id: string;
  user: User;
  study_cards: StudyCard[];
}

export interface FormCreateStudySet {
  title: string;
  description: string;
  visibility: string;
}

// Only for test
export const studySetsDataTest: StudySet[] = [
  {
    id: '6264ec61-f5d9-4e9f-965a-a2284f460c67',
    title: 'Smite Gods',
    description: 'All Smite Gods',
    visibility: 'public',
    creation_date: new Date('2020-07-29T00:11:21.359Z'),
    completation_date: null,
    original_user_id: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
    user: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
    grade: 0,
  },
  {
    id: 'b7be97eb-7e1e-4856-9f53-8d6db41385a9',
    title: 'Smite Magical Items',
    description: 'All Smite Magical Items',
    visibility: 'public',
    creation_date: new Date('2020-07-29T00:13:57.342Z'),
    completation_date: null,
    original_user_id: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
    user: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
    grade: 0,
  },
  {
    id: 'f8cd14d9-f57d-4897-aac3-545f29c16291',
    title: 'Smite Physical Items',
    description: 'All Smite Physical Items',
    visibility: 'public',
    creation_date: new Date('2020-07-29T00:14:38.258Z'),
    completation_date: null,
    original_user_id: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
    user: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
    grade: 0,
  },
];
