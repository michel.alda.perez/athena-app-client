export interface User {
  id: string;
  username: string;
  full_name: string;
  email: string;
  profile_photo: string;
  password: string;
  username_changed: boolean;
  facebook_id: string;
  google_id: string;
}

// Only for test
export const actualUserTest: User = {
  id: 'f40b5165-2f0f-43ec-a59e-3714199848b1',
  username: 'username1',
  full_name: null,
  email: 'username1@test.com',
  password: '$2b$10$sSS4UB8GRDHeJC1a4AMWDeY89.o1TjWxcHoENsWdJCU/llF0udBuq',
  profile_photo:
    'https://res.cloudinary.com/dbc1hdxqi/image/upload/v1595354981/athena-project/assets/athena-default-icon_f4swin.png',
  username_changed: false,
  google_id: null,
  facebook_id: null,
};
