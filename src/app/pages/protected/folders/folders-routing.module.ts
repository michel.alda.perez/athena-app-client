import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoldersComponent } from './folders.component';
import { FolderDetailsComponent } from './folder-details/folder-details.component';
import { FoldersResolverService } from '../../../helpers/resolvers/folders-resolver.service';
import { FolderOneResolverService } from '../../../helpers/resolvers/folder-one-resolver.service';
import { UserInfoResolverService } from 'src/app/helpers/resolvers/user-info-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: FoldersComponent,
    resolve: { myFolders: FoldersResolverService },
  },
  {
    path: ':id',
    component: FolderDetailsComponent,
    resolve: {
      currentFolder: FolderOneResolverService,
      user: UserInfoResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoldersRoutingModule {}
