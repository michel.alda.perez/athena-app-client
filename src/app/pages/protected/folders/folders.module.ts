import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoldersRoutingModule } from './folders-routing.module';
import { SharedModule } from '../../../shared/shared.module';

import { FoldersComponent } from './folders.component';
import { FolderDetailsComponent } from './folder-details/folder-details.component';

@NgModule({
  declarations: [FoldersComponent, FolderDetailsComponent],
  imports: [CommonModule, FoldersRoutingModule, SharedModule],
})
export class FoldersModule {}
