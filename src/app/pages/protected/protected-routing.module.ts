import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { StudySetsComponent } from './study-sets/study-sets.component';
import { FoldersComponent } from './folders/folders.component';
import { ProfileComponent } from './profile/profile.component';

import { UserInfoResolverService } from '../../helpers/resolvers/user-info-resolver.service';
import { StudySetResolverService } from '../../helpers/resolvers/study-set-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: { user: UserInfoResolverService },
  },
  {
    path: 'search',
    component: SearchComponent,
    resolve: { studySets: StudySetResolverService },
  },
  {
    path: 'study-sets',
    loadChildren: () =>
      import('./study-sets/study-sets.module').then(
        (mod) => mod.StudySetsModule
      ),
  },
  {
    path: 'folders',
    loadChildren: () =>
      import('./folders/folders.module').then((mod) => mod.FoldersModule),
  },
  {
    path: 'profile',
    component: ProfileComponent,
    resolve: { user: UserInfoResolverService },
  },
  { path: '**', pathMatch: 'full', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProtectedRoutingModule {}
