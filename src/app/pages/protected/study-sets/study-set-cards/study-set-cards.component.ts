import { Component, OnInit } from '@angular/core';
import { OwlOptions, SlidesOutputData } from 'ngx-owl-carousel-o';
import { StudyCard } from 'src/app/interfaces/StudyCard.interface';
import { ActivatedRoute } from '@angular/router';
import { StudySet } from 'src/app/interfaces/StudySet.interface';

@Component({
  selector: 'app-study-set-cards',
  templateUrl: './study-set-cards.component.html',
  styleUrls: ['./study-set-cards.component.scss'],
})
export class StudySetCardsComponent implements OnInit {
  options: OwlOptions;
  activeSlides: SlidesOutputData;
  position = 0;
  cards: StudyCard[];
  currentStudySetTest: StudySet;

  constructor(private router: ActivatedRoute) {}

  ngOnInit(): void {
    this.options = {
      loop: false,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      dots: true,
      navSpeed: 700,
      navText: ['Previous', 'Next'],
      responsive: {
        0: {
          items: 1,
        },
        400: {
          items: 1,
        },
        740: {
          items: 1,
        },
        940: {
          items: 1,
        },
      },
      nav: true,
    };
    this.router.data.subscribe((data) => {
      this.currentStudySetTest = data.studySet;
      this.cards = this.currentStudySetTest.study_cards;
    });
  }

  getData(data: SlidesOutputData) {
    this.position = data.startPosition;
    this.activeSlides = data;
  }
}
