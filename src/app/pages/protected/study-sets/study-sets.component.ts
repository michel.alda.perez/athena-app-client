import { Component, OnInit } from '@angular/core';
import { StudySet } from '../../../interfaces/StudySet.interface';
import { User } from 'src/app/interfaces/User.interface';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EditStudySetComponent } from 'src/app/shared/edit-dialogs/edit-study-set/edit-study-set.component';

@Component({
  selector: 'app-study-sets',
  templateUrl: './study-sets.component.html',
  styleUrls: ['./study-sets.component.scss'],
})
export class StudySetsComponent implements OnInit {
  myStudySets: StudySet[];
  myStudySetsLoaded = false;
  user: User;
  userLoaded = false;
  userPhoto: string;
  loadAgain: boolean;

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data) => {
      this.myStudySets = data.myStudySets;
      this.myStudySetsLoaded = true;
      this.user = data.user;
      this.userPhoto = this.user.profile_photo;
      this.userLoaded = true;
    });
  }

  openCreateStudySetDialog() {
    let dialogRef = this.dialog.open(EditStudySetComponent, {
      data: {
        title: 'Create New Study Set',
        content: 'Complete the fields to continue',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') this.handleCreateStudySet();
    });
  }

  handleCreateStudySet() {
    this._snackBar.open(`New Study Set Created`, 'Successfully', {
      duration: 2000,
    });
  }
}
