import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormCreateStudySet,
  ResponseGetStudySets,
  StudySet,
} from '../interfaces/StudySet.interface';

@Injectable({
  providedIn: 'root',
})
export class StudySetsService {
  baseUrl = 'https://athena-project-app.herokuapp.com/api/study_sets';

  constructor(private http: HttpClient) {}

  getStudySets() {
    return this.http.get<StudySet[]>(this.baseUrl);
  }

  getUserStudySets() {
    return this.http.get<StudySet[]>(this.baseUrl + '/user');
  }

  getOneStudySet(id: string) {
    return this.http.get<StudySet>(this.baseUrl + `/${id}`);
  }

  createStudySet(formValues: FormCreateStudySet) {
    return this.http.post<StudySet>(this.baseUrl + '/create', formValues);
  }

  editStudySet(id: string, formValues: FormCreateStudySet) {
    return this.http.post<StudySet>(this.baseUrl + `/${id}/edit`, formValues);
  }

  deleteStudySet(id: string) {
    return this.http.get(this.baseUrl + `/${id}/delete`);
  }

  saveStudySet(id: string) {
    return this.http.get<StudySet>(this.baseUrl + `/${id}/save`);
  }
}
