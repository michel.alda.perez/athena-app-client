import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { MatDialog } from '@angular/material/dialog';
import { EditStudySetComponent } from '../../edit-dialogs/edit-study-set/edit-study-set.component';
import { EditFolderComponent } from '../../edit-dialogs/edit-folder/edit-folder.component';

@Component({
  selector: 'app-title-card',
  templateUrl: './title-card.component.html',
  styleUrls: ['./title-card.component.scss'],
})
export class TitleCardComponent implements OnInit {
  @Input() title: string;
  @Input() actions: boolean;
  @Input() createStudySet: boolean;
  @Input() createFolder: boolean;
  @Input() toStudySetDetails: boolean;
  @Input() toStudySets: boolean;
  @Input() toFolders: boolean;
  @Input() StudySetId: string;

  constructor(public dialog: MatDialog, private _snackBar: MatSnackBar) {}

  ngOnInit(): void {}

  openCreateStudySetDialog() {
    let dialogRef = this.dialog.open(EditStudySetComponent, {
      data: {
        title: 'Create New Study Set',
        content: 'Complete the fields to continue',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') this.handleCreateStudySet();
    });
  }

  handleCreateStudySet() {
    this._snackBar.open(`'${this.title}'`, 'Created Successfully', {
      duration: 2000,
    });
  }

  openCreateFolderDialog() {
    let dialogRef = this.dialog.open(EditFolderComponent, {
      data: {
        title: 'Create New Folder',
        content: 'Complete the fields to continue',
        createFolder: true,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'true') this.handleCreateFolder();
    });
  }

  handleCreateFolder() {
    this._snackBar.open(`'${this.title}'`, 'Created Successfully', {
      duration: 2000,
    });
  }
}
