import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { NavigationComponent } from './navigation/navigation.component';
import { InputComponent } from './input/input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';

import { CarouselModule } from 'ngx-owl-carousel-o';

import { CardComponent } from './card/card.component';
import { TitleCardComponent } from './cards/title-card/title-card.component';
import { LandingPageSectionComponent } from './landing-page-section/landing-page-section.component';
import { SimpleViewCardComponent } from './cards/simple-view-card/simple-view-card.component';
import { EditStudySetComponent } from './edit-dialogs/edit-study-set/edit-study-set.component';
import { EditFolderComponent } from './edit-dialogs/edit-folder/edit-folder.component';
import { PreviewCardsComponent } from './edit-dialogs/preview-cards/preview-cards.component';
import { EditCardComponent } from './edit-dialogs/edit-card/edit-card.component';
import { LoadingComponent } from './loading/loading.component';

import { FilterPipe } from '../helpers/pipes/filter.pipe';
import { HighlightDirective } from '../helpers/directives/highlight.directive';
import { AddStudySetDialogComponent } from './add-study-set-dialog/add-study-set-dialog.component';

@NgModule({
  declarations: [
    NavigationComponent,
    InputComponent,
    DialogComponent,
    CardComponent,
    TitleCardComponent,
    LandingPageSectionComponent,
    SimpleViewCardComponent,
    EditStudySetComponent,
    EditFolderComponent,
    PreviewCardsComponent,
    EditCardComponent,
    LoadingComponent,
    FilterPipe,
    HighlightDirective,
    AddStudySetDialogComponent,
  ],
  entryComponents: [DialogComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SharedRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatRippleModule,
    MatDividerModule,
    MatSelectModule,
    CarouselModule,
  ],
  exports: [
    SharedRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatRippleModule,
    MatDividerModule,
    MatSelectModule,
    CarouselModule,
    NavigationComponent,
    InputComponent,
    DialogComponent,
    CardComponent,
    TitleCardComponent,
    LandingPageSectionComponent,
    SimpleViewCardComponent,
    EditStudySetComponent,
    PreviewCardsComponent,
    EditCardComponent,
    LoadingComponent,
    AddStudySetDialogComponent,
    FilterPipe,
    HighlightDirective,
  ],
})
export class SharedModule {}
